const { response } = require("express");
const express = require("express");
const { use } = require("express/lib/router");
const app = express();

app.use(express.json());

// 1º.) parâmetro de envio via query (atributo nome)
app.get("/clientes", (req, res) => {
    let query = req.query;
  
    res.send("recebido " + query.nome);

  });

//***************************************************************************** */

 
app.post("/clientes", (req, res) => {
    const header = req.headers;
    const body = req.body;
    const retorno = {access:header.access, body:body};

    if (header.access == "Correia"){
        res.send(retorno);  
    }else{
        res.send("Inválido");
    }
});

//***************************************************************** */


app.delete("/clientes/:id", (req, res) => {
    const header = req.headers;
    const params = req.params;
  
    const retorno = { access: header.access, id: params.id };
  
    if (header.access == "Correia") {
      res.send(retorno);
  
    } else {
      res.send("INVÁLIDO");
    }
  });

  //******************************************************************************* */

  app.put("/clientes", (req, res) => {
    const header = req.headers;
    const body = req.body;
  
    const retorno = { access: header.access, body: body };
  
    if (header.access == "Correia") {
      res.send(retorno);
  
    } else {
      res.send("INVÁLIDO");
    }
  });
 
  /*FUNCIONÁRIOS*/

  app.get("/funcionarios", (req, res) => {
    let query = req.query;
  
    res.send("recebido " + query.nome);

  });

//***************************************************************************** */

 
app.post("/funcionarios", (req, res) => {
    const header = req.headers;
    const body = req.body;
    const retorno = {access:header.access, body:body};

    if(header.access == "Correia"){
        res.send(retorno);  
    }else{
        res.send("Inválido");
    }
});

//***************************************************************** */


app.delete("/funcionarios/:id", (req, res) => {
    const header = req.headers;
    const params = req.params;
  
    const retorno = { access: header.access, id: params.id };
  
    if (header.access == "Correia") {
      res.send(retorno);
  
    } else {
      res.send("INVÁLIDO");
    }
  });

  //******************************************************************************* */

  app.put("/funcionarios", (req, res) => {
    const header = req.headers;
    const body = req.body;
  
    const retorno = { access: header.access, body: body };
  
    if (header.access == "Correia") {
      res.send(retorno);
  
    } else {
      res.send("INVÁLIDO");
    }
  });

const PORT = 8000;
app.listen(PORT, () => {
  console.log("rodando na porta", PORT);
});